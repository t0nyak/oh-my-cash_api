import { Router } from 'express';
const router = Router();
import {
  findAll,
  findAllActive,
  findOne,
  deleteOne,
  update,
} from '../controllers/user.controller.js';
import auth from '../middlewares/auth.js';

router.get('/', auth, findAll);
router.get('/active', findAllActive);
router.get('/:id', findOne);
router.put('/update/:id', update);
router.delete('/delete/:id', deleteOne);

export default router;
