import config from '../config/config.js';

import mongoose from 'mongoose';

const db = {};
db.mongoose = mongoose;
db.url = config.url;

export default db;
