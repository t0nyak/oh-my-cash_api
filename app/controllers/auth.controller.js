import User from '../models/user.model.js';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import config from '../config/config.js';
import { validationResult } from 'express-validator';

export async function register(req, res) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).jsonp(errors.array());
  }

  try {
    const passHash = await bcrypt.hash(req.body.password, 10);

    const user = new User({
      email: req.body.email,
      password: passHash,
      active: false,
    });

    const result = await user.save();

    res.status(201).json({
      message: 'User successfully created!',
      result,
    });
  } catch (error) {
    res.status(500).json({
      error,
    });
  }
}

export async function login(req, res) {
  if (!req.body.email || !req.body.password) {
    res
      .status(400)
      .json({ msg: 'You need to provide an email and a password' });
    return;
  }

  try {
    const user = await User.findOne({
      email: req.body.email,
    });

    const result = await bcrypt.compare(req.body.password, user.password);

    if (!result) {
      res.status(401).json({ message: 'Authenticatiosn failed' });
    } else {
      const token = jwt.sign(
        {
          email: user.email,
          userId: user.id,
        },
        config.secret,
        {
          expiresIn: '1h',
        }
      );
      user.password = undefined;

      res.status(200).json({
        token,
        expiresIn: 3600,
        user,
      });
    }
  } catch (error) {
    res.status(401).json({ message: 'Authentication failed', error });
  }
}

export async function activate(req, res, next) {
  try {
    const result = User.findByIdAndUpdate(req.params.id, {
      $set: {
        active: true,
      },
    });

    res.status(200).json({
      message: 'The user was activated successfully',
    });
  } catch (error) {
    return next(error);
  }
}
