import mongoose from 'mongoose';
const { Schema, model } = mongoose;
import uniqueValidator from 'mongoose-unique-validator';

let schema = Schema(
  {
    email: {
      type: String,
      unique: true,
    },
    password: String,
    username: String,
    active: Boolean,
    avatar: String,
    sexo: String,
    birth_date: Date,
  },
  { timestamps: true, collection: 'users' }
);

schema.plugin(uniqueValidator, { message: 'Email already in use.' });

const User = model('user', schema);
export default User;
