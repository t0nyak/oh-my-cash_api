import jwt from 'jsonwebtoken';
import config from '../config/config.js';

export default (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    jwt.verify(token, config.secret);
    next();
  } catch (error) {
    res.status(401).json({ message: 'Authentication failed!' });
  }
};
