import User from '../models/user.model.js';

export async function findAll(req, res, next) {
  try {
    const users = await User.find();

    res.status(200).json(users);
  } catch (error) {
    return next(error);
  }
}

export async function findOne(req, res, next) {
  try {
    const user = await User.findById(req.params.id);

    res.status(200).json(user);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const updatedUser = await User.findByIdAndUpdate(req.params.id, {
      $set: req.body,
    });

    res.status(200).json({ user: updatedUser });
  } catch (error) {
    return next(error);
  }
}

export async function deleteOne(req, res) {
  try {
    const result = await User.findByIdAndDelete(req.params.id);

    res.status(200).json({ result });
  } catch (error) {
    return next(error);
  }
}

export async function findAllActive(req, res, next) {
  try {
    const users = await User.find({ active: true });

    res.status(200).json(users);
  } catch (error) {
    return next(error);
  }
}
