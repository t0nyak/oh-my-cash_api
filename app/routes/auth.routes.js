import { Router } from 'express';
const router = Router();
import { register, login, activate } from '../controllers/auth.controller.js';
import { check } from 'express-validator';

router.post(
  '/register',
  [
    check('email', 'Email is required').not().isEmpty(),
    check('password', 'Password should be between 5 to 8 characters long')
      .not()
      .isEmpty()
      .isLength({ min: 4 }),
  ],
  register
);
router.post('/login', login);
router.post('/activate/:id', activate);

export default router;
